#include "Data.h"
#include "Bitmap.h"
#include "GMath.h"
#include "Figures.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../stb/stb_image_write.h"
#define STB_IMAGE_IMPLEMENTATION
#include "../stb/stb_image.h"


using namespace std;
std::vector<std::unique_ptr<Object>> objects; 
std::vector<std::unique_ptr<Light>> lights;

const Vec3 STEEL = Vec3(0.4, 0.4, 0.4);
const Vec3 GOLD = Vec3(0.5, 0.4, 0.1);
const Vec3 BROWN = Vec3(0.4, 0.4, 0.3);
const Vec3 GREEN = Vec3(0, 0.6, 0);
const Vec3 BLUE = Vec3(0, 0, 0.8);
const Vec3 MINT = Vec3(0, 0.8, 0.8);
const Vec3 BLACK = Vec3(0.0, 0.0, 0.0);
const Vec3 MIRROR = Vec3(0.0, 10.0, 0.8);
const Vec3 GLASS = Vec3(0.0, 10.0, 0.8);

const Material steel(20.0, 1.5, STEEL, GLOSSY);
const Material brown(50.0, 1.5, BROWN, GLOSSY);
const Material gold(20.0, 1.5, GOLD, GLOSSY);
const Material green(20.0, 1.5, GREEN, GLOSSY);
const Material checker(5.0, 1.5, BLACK, GLOSSY);
const Material checker_d(5.0, 1.5, BLACK, DIFFUSE);
const Material blue(20.0, 1.5, BLUE, DIFFUSE);
const Material mint(20.0, 1.5, MINT, DIFFUSE);
const Material mirror(1.0, 1.5, MIRROR, REFLECTION);
const Material glass(1.0, 1.5, GLASS, REFLECTION_AND_REFRACTION);

void scene_handler(int sceneId, Preference &preference){
    Vec3 aa = Vec3(-1, -7, -4);
    Vec3 bb = Vec3(-4, -7, -8);
    Vec3 cc = Vec3(2, -7, -8);
    Vec3 hh = Vec3(-1, -1, -6);
    Vec3 a1 = Vec3(-7, 10, 4);
    Vec3 b1 = Vec3(-6, 10, -30);
    Vec3 c1 = Vec3(-7, -50, -17);
    Vec3 a2 = Vec3(7, 10, 4);
    Vec3 b2 = Vec3(6, 10, -30);
    Vec3 c2 = Vec3(7, -50, -17);
    Vec3 a3 = Vec3(-20, 5, 0);
    Vec3 b3 = Vec3(20, 5, 0);
    Vec3 c3 = Vec3(0, 5, -50);
    Vec3 a4 = Vec3(-15, -6, -15);
    Vec3 b4 = Vec3(15, -6, -15);
    Vec3 c4 = Vec3(0, 70, -15);
    switch (sceneId){
    case 1:
        preference.set_anti_aliasing(4);
        preference.set_enviroment_map(0);
        objects.push_back(std::unique_ptr<Object>(new Sphere(Vec3(-2, 0, -13), 3, brown)));
        objects.push_back(std::unique_ptr<Object>(new Cone (Vec3(2, -4, -7), 3 ,4, steel)));
        objects.push_back(std::unique_ptr<Object>(new Plane (Vec3(0,-4, 0),Vec3(0,1,0 ),checker)));
        objects.push_back(std::unique_ptr<Object>(new Sphere(Vec3(3, 0, -4), 1, glass)));
        lights.push_back(std::unique_ptr<Light>(new StandartLight(Vec3(-20, 20, 20), 1, Vec3(1, 1, 1))));
        lights.push_back(std::unique_ptr<Light>(new StandartLight(Vec3(-30, 20, 25), 1, Vec3(1, 1, 1))));
        break;
    case 2:
        preference.set_enviroment_map(1);
        preference.set_anti_aliasing(2);
        objects.push_back(std::unique_ptr<Object>(new Sphere(Vec3(1, -1, -3), 1, green)));
        objects.push_back(std::unique_ptr<Object>(new Sphere(Vec3(-2, -1, -5), 1.25, gold)));
        objects.push_back(std::unique_ptr<Object>(new Plane (Vec3(0,-4, 0), Vec3(0,1,0 ), checker)));
        objects.push_back(std::unique_ptr<Object>(new Sphere(Vec3(1.3, 2, -5), 1, blue)));
        objects.push_back(std::unique_ptr<Object>(new Sphere(Vec3(0, -1, -25), 10, mirror)));
        objects.push_back(std::unique_ptr<Object>(new Triangle(aa,bb,hh, steel, -1)));
        objects.push_back(std::unique_ptr<Object>(new Triangle(aa,cc,hh, steel, 1)));
        objects.push_back(std::unique_ptr<Object>(new Triangle(bb,cc,hh, steel, -1)));
        lights.push_back(std::unique_ptr<Light>(new StandartLight(Vec3(20, 20, 20), 1, Vec3(1, 1, 1))));
        lights.push_back(std::unique_ptr<Light>(new StandartLight(Vec3(10, 20, 25), 1, Vec3(1, 1, 1)))); 
        break;
    default:
        preference.set_anti_aliasing(1);
        preference.set_enviroment_map(2);
        objects.push_back(std::unique_ptr<Object>(new Plane (Vec3(0,-4, 0), Vec3(0,1,0 ), checker)));
        objects.push_back(std::unique_ptr<Object>(new Sphere(Vec3(2, 0, -5), 1, mirror)));
        objects.push_back(std::unique_ptr<Object>(new Sphere(Vec3(-3, 4, -12), 2, brown)));
        objects.push_back(std::unique_ptr<Object>(new Triangle(a1,b1,c1, steel, -1)));
        objects.push_back(std::unique_ptr<Object>(new Triangle(a2,b2,c2, steel, 1)));
        objects.push_back(std::unique_ptr<Object>(new Triangle(a3,b3,c3, steel, -1)));
        objects.push_back(std::unique_ptr<Object>(new Triangle(a4,b4,c4, steel, 1)));
        lights.push_back(std::unique_ptr<Light>(new StandartLight(Vec3(-7, 15, 20), 1.5, Vec3(0.85,0.7, 0.6))));
        // lights.push_back(std::unique_ptr<Light>(new StandartLight(Vec3(9, -3, 20), 1, Vec3(0.85,0.7, 0.6))));
        lights.push_back(std::unique_ptr<Light>(new StandartLight(Vec3(-9, -3, 20), 1.2, Vec3(0.85,0.7, 0.6))));
        break;
    } 
}

bool intersections(const Vec3 &originaly, const Vec3 &direction, Vec3 &hit, Vec3 &N, Material &material){
    float objects_distance = std::numeric_limits<float>::max();
    for (size_t i = 0; i < objects.size(); i++){
        float dist_i;
        if (objects[i] -> intersection(originaly, direction, dist_i) && dist_i < objects_distance){
            objects_distance = dist_i;
            hit = originaly + direction * dist_i;
            objects[i] -> get_data(hit, N, material);
        }
    }
    return objects_distance < 1000;
}

void glossy_ray(const Vec3 &direction, Material material, Vec3 N, Vec3 target_hit, Preference preference, int depth, const std::vector<Vec3> &enviroment_map, Vec3 &phong_color){
    float kr;
    gmath.f_n(direction, N, material.get_refract(), kr);
    Vec3 tmp = gmath.reflect(direction, N);
    Vec3 reflect_direction = gmath.normalization(tmp);
    Vec3 reflect_originaly = (gmath.scalar(reflect_direction, N) < 0) ? target_hit - N * 1e-4 : target_hit + N * 1e-4;
    Vec3 reflect_color = shoot_rays(reflect_originaly, reflect_direction, preference, enviroment_map, depth + 1);
    Vec3 diff = 0;
    Vec3 spec = 0;
    Vec3 shadow_originaly = (gmath.scalar(direction, N) < 0) ? target_hit + N * 1e-4 : target_hit - N * 1e-4;
    for (uint32_t i = 0; i < lights.size(); ++i)
    {
        shadow_originaly = (gmath.scalar(direction, N) < 0) ? target_hit + N * 1e-4 : target_hit - N * 1e-4;
        Vec3 direction_light, intensity_light;
        Vec3 shadow_point, shadow_N;
        float sidtance_light;
        Material tmp;
        lights[i]->get_data_light(target_hit, direction_light, intensity_light, sidtance_light);
        if (intersections(shadow_originaly, direction_light, shadow_point, shadow_N, tmp) && gmath.norma(shadow_point-shadow_originaly) < sidtance_light)
            continue;
        Vec3 reflection_direction = gmath.reflect(-direction_light, N);
        diff  += intensity_light * std::max(0.f, gmath.scalar(direction_light,N));
        spec += intensity_light * powf(std::max(0.f, -gmath.scalar(reflection_direction, direction)), material.get_specular()) ;
    }
    phong_color = diff*material.get_diffuse_color()*0.8 + material.get_diffuse_color()*spec*0.4 + material.get_diffuse_color()*reflect_color*0.40;
}

void refl_refr_ray(const Vec3 &direction, Material material, Vec3 N, Vec3 target_hit, Preference preference, int depth, const std::vector<Vec3> &enviroment_map, Vec3 &phong_color){
    float kr;
    gmath.f_n(direction, N, material.get_refract(), kr);
    Vec3 reflect_direction = gmath.normalization(gmath.reflect(direction, N));
    Vec3 refract_direction = gmath.normalization(gmath.refraction(direction, N, material.get_refract()));
    Vec3 reflect_originaly = (gmath.scalar(reflect_direction, N) < 0) ? target_hit - N * 1e-3 : target_hit + N * 1e-3;
    Vec3 refract_originaly = (gmath.scalar(refract_direction, N) < 0) ? target_hit - N * 1e-3 : target_hit + N * 1e-3;
    Vec3 reflect_color = shoot_rays(reflect_originaly, reflect_direction, preference, enviroment_map, depth + 1);
    Vec3 refract_color = shoot_rays(refract_originaly, refract_direction, preference, enviroment_map, depth + 1);
    Vec3 spec = powf(std::max(0.f, -gmath.scalar(direction, direction)), material.get_specular()) ;
    phong_color = reflect_color * kr + refract_color * (1 - kr) + material.get_diffuse_color()*spec*0.4;
}

void refl_ray(const Vec3 &direction, Vec3 N, Vec3 hit_point, Preference preference, int depth, const std::vector<Vec3> &enviroment_map, Vec3 &phong_color){
    Vec3 reflect_direction = gmath.normalization(gmath.reflect(direction, N));
    Vec3 reflect_originaly = (gmath.scalar(reflect_direction, N) < 0) ? hit_point - N * 1e-3 : hit_point + N * 1e-3;
    Vec3 reflect_color = shoot_rays(reflect_originaly, reflect_direction, preference, enviroment_map, depth + 1);
    phong_color += reflect_color * 0.8;
}

void dif_ray(const Vec3 &direction, Material material, Vec3 N, Vec3 target_hit, Vec3 &phong_color){
    Vec3 diff = 0;
    Vec3 spec = 0;
    Vec3 shadow_originaly = (gmath.scalar(direction, N) < 0) ? target_hit + N * 1e-4 : target_hit - N * 1e-4; 
    for (uint32_t i = 0; i < lights.size(); ++i){
        Vec3 shadow_orig = (gmath.scalar(direction, N) < 0) ? target_hit + N * 1e-4 : target_hit - N * 1e-4;
        Vec3 direction_light;
        Vec3 intensity_light;
        Vec3 shadow_point;
        Vec3 shadow_N;
        float distance_light;                    
        Material tmp;
        lights[i] -> get_data_light(target_hit, direction_light, intensity_light, distance_light);
        if (intersections(shadow_orig, direction_light, shadow_point, shadow_N, tmp) && gmath.norma(shadow_point-shadow_orig) < distance_light)
            continue;
        Vec3 reflection_direction = gmath.reflect(-direction_light, N);
        diff  += intensity_light * std::max(0.f, gmath.scalar(direction_light,N));
        spec += intensity_light * powf(std::max(0.f, -gmath.scalar(reflection_direction, direction)), material.get_specular()) ;
    }
    phong_color = diff * material.get_diffuse_color() * 0.8 + spec * 0.2; 
}

Vec3 shoot_rays(const Vec3 &originaly, const Vec3 &direction, Preference &preference, const std::vector<Vec3> &enviroment_map, size_t depth = 0) {
    Vec3 target_hit;
    Vec3 N;
    Material material;
    Vec3 phong_color = 0;
    if ((depth > preference.get_maxDepth()) || (!(intersections(originaly, direction, target_hit, N, material)))){   
        if (preference.get_enviroment_map() == 0){
            return preference.get_backgroundColor();
        } 
        Sphere env(Vec3(0,0,0), 1000, Material());
        float dist = 0;
        env.intersection(originaly, direction, dist);
        Vec3 p = originaly+direction*dist;
        int a = (atan2(p.get_z(), p.get_x())/(2*M_PI) + .5)**preference.get_enviroment_map_width();
        int b = acos(p.get_y()/1000)/M_PI**preference.get_enviroment_map_height();
        return enviroment_map[a+b**preference.get_enviroment_map_width()];
    }
    if (intersections(originaly, direction, target_hit, N, material)){
        switch (material.get_material_type()){
            case GLOSSY:{
                glossy_ray(direction, material, N, target_hit, preference, depth, enviroment_map, phong_color);
                break;
            }
            case REFLECTION_AND_REFRACTION:{
                refl_refr_ray(direction, material, N, target_hit, preference, depth, enviroment_map, phong_color);
                break;
            }
            case REFLECTION:{
                refl_ray(direction, N, target_hit, preference, depth, enviroment_map, phong_color);
                break;
            }
            default:{
                dif_ray(direction, material, N, target_hit, phong_color); 
                break;
            }
        }
    }
    return phong_color;
}

int main(int argc, const char **argv){
    std::unordered_map<std::string, std::string> cmdLineParams;
    for (int i = 0; i < argc; i++)
    {
        std::string key(argv[i]);

        if (key.size() > 0 && key[0] == '-'){
            if (i != argc - 1)
            {
                cmdLineParams[key] = argv[i + 1];
                i++;
            }else{
                cmdLineParams[key] = "";
            }
        }
    }

    std::string outFilePath = "zout.bmp";
    if (cmdLineParams.find("-out") != cmdLineParams.end())
        outFilePath = cmdLineParams["-out"];

    int scene_id = 0;
    if (cmdLineParams.find("-scene") != cmdLineParams.end())
        scene_id = atoi(cmdLineParams["-scene"].c_str());

    int num_threads = 1;
    if (cmdLineParams.find("-threads") != cmdLineParams.end())
        num_threads = atoi(cmdLineParams["-threads"].c_str());

    Preference preference;
    scene_handler(scene_id, preference);
    std::vector<Vec3> enviroment_map;
    if (preference.get_enviroment_map() > 0){
        std::string enviroment_map_path = "";
        enviroment_map_path += "../envmap" + to_string(preference.get_enviroment_map()) + (string)".hdr";
        int n = -1;
        unsigned char *pixel_map = stbi_load(enviroment_map_path.c_str(), preference.get_enviroment_map_width(), preference.get_enviroment_map_height(), &n, 0);
        if (!pixel_map || n != 3) {
            std::cerr << "Error: can not load the environment map" << std::endl;
            return -1;
        }
        enviroment_map = std::vector<Vec3>(*preference.get_enviroment_map_width()**preference.get_enviroment_map_height());
#pragma omp parallel for num_threads(num_threads)
        for (int j = *preference.get_enviroment_map_height()-1; j>=0 ; j--) {
            for (int i = 0; i<*preference.get_enviroment_map_width(); i++) {
                enviroment_map[i+j**preference.get_enviroment_map_width()] = Vec3(pixel_map[(i+j**preference.get_enviroment_map_width())*3+0], pixel_map[(i+j**preference.get_enviroment_map_width())*3+1], pixel_map[(i+j**preference.get_enviroment_map_width())*3+2])*(1/255.);
            }
        }
        stbi_image_free(pixel_map);
    }
    std::vector<uint32_t> image(preference.get_height() * preference.get_width());
    float scale = tan(gmath.deg_conv(preference.get_fov() * 0.5));
    float image_aspect_ratio = preference.get_width() / preference.get_height();
#pragma omp parallel for num_threads(num_threads)
    for (size_t j = 0; j < preference.get_height(); j++){
        for (size_t i = 0; i < preference.get_width(); i++){
            Vec3 tmp = Vec3(0,0,0);
            for (size_t k = 0; k < preference.get_anti_aliasing(); k++){
                float x = (2 * (i + 0.5 + k*0.25) / (float)preference.get_width() -  1) * image_aspect_ratio * scale ;
                float y = (2 * (j + 0.5 - k*0.25) / (float)preference.get_height() - 1) * scale; 
                Vec3 tmp1 = Vec3(x, y, -1);
                Vec3 dir = gmath.normalization(tmp1);
                tmp += shoot_rays(Vec3(0, 0, 0), dir, preference, enviroment_map);
            }
            tmp = tmp * (1.0 / preference.get_anti_aliasing());
            float max = std::max(tmp.get_x(), std::max(tmp.get_y(), tmp.get_z()));
            if (max > 1)
                tmp = tmp * (1. / max);
            image[i + j * preference.get_width()] = (uint32_t)(255 * std::max(0.f, std::min(1.f, tmp.get_z()))) << 16 | (uint32_t)(255 * std::max(0.f, std::min(1.f,tmp.get_y()))) << 8 | (uint32_t)(255 * std::max(0.f, std::min(1.f, tmp.get_x())));
        }
    }
    save_bmp(outFilePath.c_str(), image.data(), preference.get_width(), preference.get_height());
}