#include <vector>
#include <fstream>
#include <cstring>

struct Pixel { unsigned char r, g, b; };

void write_bmp(const char* file_name, Pixel* a_pixel_data, int width, int height){
  int padded_size = (width*height) * sizeof(Pixel);
  unsigned char bmp_file_header[14] = {'B','M',0,0,0,0,0,0,0,0,54,0,0,0};
  unsigned char bmp_info_header[40] = {40,0,0,0,0,0,0,0,0,0,0,0,1,0,24,0};
  bmp_file_header[2] = (unsigned char)(padded_size);
  bmp_file_header[3] = (unsigned char)(padded_size >> 8);
  bmp_file_header[4] = (unsigned char)(padded_size >> 16);
  bmp_file_header[5] = (unsigned char)(padded_size >> 24);
  bmp_info_header[4] = (unsigned char)(width);
  bmp_info_header[5] = (unsigned char)(width >> 8);
  bmp_info_header[6] = (unsigned char)(width >> 16);
  bmp_info_header[7] = (unsigned char)(width >> 24);
  bmp_info_header[8] = (unsigned char)(height);
  bmp_info_header[9] = (unsigned char)(height >> 8);
  bmp_info_header[10] = (unsigned char)(height >> 16);
  bmp_info_header[11] = (unsigned char)(height >> 24);
  std::ofstream out(file_name, std::ios::out | std::ios::binary);
  out.write((const char*)bmp_file_header, 14);
  out.write((const char*)bmp_info_header, 40);
  out.write((const char*)a_pixel_data, padded_size);
  out.flush();
  out.close();
}

void save_bmp(const char* file_name, const unsigned int* pixels, int width, int height){
  std::vector<Pixel> pixels2(width*height);
  for (size_t i = 0; i < pixels2.size(); i++){
    Pixel px;
    px.r = (pixels[i] & 0x00FF0000) >> 16;
    px.g = (pixels[i] & 0x0000FF00) >> 8;
    px.b = (pixels[i] & 0x000000FF);
    pixels2[i] = px;
  }
  write_bmp(file_name, &pixels2[0], width, height);
}
