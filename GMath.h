#pragma once
#include "Data.h"
using namespace std;

class GMath {
public:
    float scalar(const Vec3 &a, const Vec3 &b){ return a.get_x() * b.get_x() + a.get_y() * b.get_y() + a.get_z() * b.get_z(); }
    float deg_conv(const float &deg){ return deg * M_PI / 180; }
    float norma(const Vec3 &v){ return sqrt(scalar(v,v)); }
    float clamp(const float &lo, const float &hi, const float &v){ return max(lo, min(hi, v)); } 
    Vec3 crossVec(const Vec3 &a, const Vec3 &b){ return Vec3(a.get_y() * b.get_z() - a.get_z() * b.get_y(), a.get_z() * b.get_x() - a.get_x() * b.get_z(), a.get_x() * b.get_y() - a.get_y() *  b.get_x()); }
    Vec3 reflect(const Vec3 &I, const Vec3 &N){ return I -  ((N* scalar(N,I) * 2)); }  

    bool solve_x(const float &a, const float &b, const float &c, float &x1, float &x2){
        float discriminant = b * b - 4 * a * c;
        if (discriminant < 0)
            return false;
        else if (discriminant == 0)
            x1 = x2 = -0.5 * b / a;
        else{
            float q = (b > 0) ? -0.5 * (b + sqrt(discriminant)) : -0.5 * (b - sqrt(discriminant));
            x1 = q / a;
            x2 = c / q;
        }
        if (x1 > x2)
            swap(x1, x2);
        return true;
    }
    
    void f_n(const Vec3 &I, const Vec3 &N, const float &ior, float &kr) { 
        float cos_i = clamp(-1, 1, scalar(I, N)); 
        float eta_i = 1;
        float eta_t = ior; 
        if (cos_i > 0) {
            swap(eta_i, eta_t); 
        } 
        float sint = eta_i / eta_t * sqrtf(max(0.f, 1 - cos_i * cos_i)); 
        if (sint >= 1) { 
            kr = 1; 
        } 
        else { 
            float cost = sqrtf(max(0.f, 1 - sint * sint)); 
            cos_i = fabsf(cos_i); 
            float Rs = ((eta_t * cos_i) - (eta_i * cost)) / ((eta_t * cos_i) + (eta_i * cost)); 
            float Rp = ((eta_i * cos_i) - (eta_t * cost)) / ((eta_i * cos_i) + (eta_t * cost)); 
            kr = (Rs * Rs + Rp * Rp) / 2; 
        } 
    } 

    Vec3 normalization(const Vec3 &v) { 
        float v2 = scalar(v, v); 
        if (v2 > 0) { 
            float inv_mag = 1 / sqrtf(v2); 
            return Vec3(v.get_x() * inv_mag, v.get_y() * inv_mag, v.get_z() * inv_mag); 
        } 
        return v; 
    }

    Vec3 refraction(const Vec3 &I, const Vec3 &N, const float &refractive_index) 
    { 
        float cos_i = -max(-1.f, min(1.f, scalar(I,N)));
        float eta_i = 1;
        float eta_t = refractive_index;
        Vec3 n = N;
        if (cos_i < 0) { 
            cos_i = -cos_i;
            swap(eta_i, eta_t); 
            n = -N;
        }
        float eta = eta_i / eta_t;
        float k = 1 - eta*eta*(1 - cos_i*cos_i);
        return k < 0 ? Vec3(0,0,0) : I*eta + n*(eta * cos_i - sqrtf(k));
    }
}gmath;