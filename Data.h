#pragma once
#include <iostream>
#include <cmath>
#include <limits>
#include <ctime>
#include <cstdint>
#include <cstring>
#include <unordered_map>
#include <memory>
#include <vector>

struct Vec3{
private:
    float x;
    float y;
    float z;

public:
    Vec3() : x(0), y(0), z(0) {}
    Vec3(float xx) : x(xx), y(xx), z(xx) {}
    Vec3(float xx, float yy, float zz) : x(xx), y(yy), z(zz) {}
    Vec3 operator+(const Vec3 &v) const { return Vec3(x + v.x, y + v.y, z + v.z); }
    Vec3 operator-(const Vec3 &v) const { return Vec3(x - v.x, y - v.y, z - v.z); }
    Vec3 operator-() const { return Vec3(-x, -y, -z); }
    Vec3 operator*(const Vec3 &v) const { return Vec3(x * v.x, y * v.y, z * v.z); }
    Vec3 operator*(const float &r) const { return Vec3(x * r, y * r, z * r); }
    Vec3 &operator+=(const Vec3 &v){
        x += v.x, y += v.y, z += v.z;
        return *this;
    }
    friend std::ostream &operator<<(std::ostream &s, const Vec3 &v){
        return s << v.x << ", " << v.y << ", " << v.z;
    }

    void set_x(float xx){ x = xx; }
    void set_y(float yy){ y = yy; }
    void set_z(float zz){ z = zz; }

    float get_x()const{ return x; }
    float get_y()const{ return y; }
    float get_z()const{ return z; }
};

struct Preference
{
private:
    short width;
    short height;
    short max_Depth;
    float fov;
    float bias;
    float anti_aliasing;
    int enviroment_map;
    int enviroment_map_width;
    int enviroment_map_height;
    Vec3 backgroundColor;

public:
    Preference()
    {
        width = 704;
        height = 576;   
        fov = 90;
        max_Depth = 4;
        anti_aliasing = 1;
        backgroundColor = Vec3(0.0, 0.0, 0.0);
        enviroment_map = 0;
    }

    void set_width(short t){ width = t; }
    void set_height(short t){ height = t;}
    void set_max_Depth(short t){ max_Depth = t; }
    void set_fov(float t){ fov = t; }
    void set_bias(float t){ bias = t; }
    void set_anti_aliasing(float t){ anti_aliasing = t; }
    void set_backgroundColor(Vec3 t){ backgroundColor = t; }
    void set_enviroment_map(int t) { enviroment_map = t; }

    short get_width(){ return width; }
    short get_height(){ return height;}
    short get_maxDepth(){ return max_Depth; }
    float get_fov(){ return fov; }
    float get_bias(){ return bias; }
    float get_anti_aliasing(){ return anti_aliasing; }
    int get_enviroment_map() { return enviroment_map; }
    int* get_enviroment_map_width() { return &enviroment_map_width; }
    int* get_enviroment_map_height() { return &enviroment_map_height; }
    Vec3 get_backgroundColor(){ return backgroundColor; }
};

enum MaterialType {GLOSSY, DIFFUSE, REFLECTION , REFRACTION, DIFFUSE_REFLECTION, REFLECTION_AND_REFRACTION};

struct Material {
protected:
    float specular;
    float refract;
    Vec3 diffuse_color;
    MaterialType materialType;

public:
    Material(const float &s, const float &r, const Vec3 &color ,const MaterialType &m) : specular(s),refract(r),diffuse_color(color), materialType(m){ }
    Material() : specular(), refract(), diffuse_color(), materialType(DIFFUSE) {}
    
    void set_specular(float t){ specular = t; }
    void set_refract(float t){ refract = t; }
    void set_diffuse_color(Vec3 t){ diffuse_color = t; }
    void set_material_type(MaterialType t){ materialType = t; }

    float get_specular(){ return specular; }
    float get_refract(){ return refract; }
    Vec3 get_diffuse_color(){ return diffuse_color; }
    MaterialType get_material_type(){ return materialType; }
};

class Light {
public:
    Light() {}
    virtual ~Light() {}
    virtual void get_data_light(const Vec3 &, Vec3 &, Vec3 &, float &) const = 0;
};

Vec3 shoot_rays(const Vec3 &orig, const Vec3 &dir, Preference &preference, const std::vector<Vec3> &envmap, size_t depth);
void scene_handler(int sceneId, Preference &preference);
bool intersections(const Vec3 &orig, const Vec3 &dir, Vec3 &hit, Vec3 &N, Material &material);
void glossy_ray(const Vec3 &dir, Material material, Vec3 N, Vec3 hit_point, Preference preference, int depth, const std::vector<Vec3> &envmap, Vec3 &PhongColor);
void refl_refr_ray(const Vec3 &dir, Material material, Vec3 N, Vec3 hit_point, Preference preference, int depth, const std::vector<Vec3> &envmap, Vec3 &PhongColor);
void refl_ray(const Vec3 &dir, Vec3 N, Vec3 hit_point, Preference preference, int depth, const std::vector<Vec3> &envmap, Vec3 &PhongColor);
void dif_ray(const Vec3 &dir, Material material, Vec3 N, Vec3 hit_point, Vec3 &PhongColor);


