#pragma once
#include "Data.h"
#include "GMath.h"

class Object { 
public: 
    Object() {} 
    virtual ~Object() {}
    virtual bool intersection(const Vec3 &, const Vec3 &, float &) const=0; 
    virtual void get_data(const Vec3 &, Vec3 &, Material &) const=0; 
}; 

class Sphere : public Object{
protected:
    Vec3 center;
    float radius;
    Material material;
    
public:
    Sphere(const Vec3 &centr, const float &rad, const Material &mat) : center(centr), radius(rad), material(mat){};

    bool intersection(const Vec3 &originaly, const Vec3 &direction, float &tnear) const{ 
        Vec3 L = originaly - center; 
        float a = gmath.scalar(direction, direction); 
        float b = 2 * gmath.scalar(direction, L); 
        float c = gmath.scalar(L, L) - (radius*radius); 
        float t0;
        float t1; 
        if (!gmath.solve_x(a, b, c, t0, t1)) 
            return false; 
        if (t0 < 0) 
            t0 = t1; 
        if (t0 < 0) 
            return false; 
        tnear = t0; 
        return true; 
    }

    void get_data(const Vec3 &target_hit, Vec3 &N, Material &mat) const{
        Vec3 t = target_hit - center;
        N = gmath.normalization(t); 
        mat = material;
    }
};

class Cone : public Object{
private:
    Vec3 center;
    float radius;
    float height;
    Material material;

public:
    Cone(const Vec3 &centr, const float &raf, const float &high, const Material &mat) : center(centr), radius(raf), height(high), material(mat){};

    bool intersection(const Vec3 &originaly, const Vec3 &direction, float &tnear) const{
        float tangent = radius/height;
        float a = (direction.get_x() * direction.get_x()) + (direction.get_z() * direction.get_z()) - ((tangent * tangent)* (direction.get_y()*direction.get_y()));
        float b = (2*(originaly.get_x()-center.get_x())*direction.get_x()) + (2*(originaly.get_z() - center.get_z())*direction.get_z()) + (2*tangent*tangent*(height - originaly.get_y() + center.get_y())*direction.get_y());
        float c = ((originaly.get_x() - center.get_x())*(originaly.get_x() - center.get_x())) + ((originaly.get_z() - center.get_z())*(originaly.get_z() - center.get_z())) - (tangent*tangent*(( height - originaly.get_y() + center.get_y())*( height - originaly.get_y() + center.get_y())));
        float t0;
        float t1;
        if (!gmath.solve_x(a, b, c, t0, t1))
            return false;
        if (t0 < 0)
            if (t1 < 0)
                return false;
            else t0 = t1;
        float r = originaly.get_y() + t0 * direction.get_y();
        if (!((r >= center.get_y()) and (r <= center.get_y() + height)))
            return false;
        tnear = t0;
        return true;
    }

    void get_data(const Vec3 &target_hit, Vec3 &N, Material &mat) const {
        float r = sqrt((target_hit.get_x()-center.get_x())*(target_hit.get_x()-center.get_x()) + (target_hit.get_z()-center.get_z())*(target_hit.get_z()-center.get_z()));
        N = gmath.normalization(Vec3 (target_hit.get_x()-center.get_x(), r*(radius/height), target_hit.get_z()-center.get_z()));
        mat = material;
    }
};

class Triangle : public Object
{
protected: 
    Vec3 v0;
    Vec3 v1;
    Vec3 v2;
    Material material;
    int norm;

public:
    Triangle (const Vec3 &a,const Vec3 &b,const Vec3 &c, const Material &m, int i) : v0(a), v1(b), v2(c), material(m), norm(i){}

    bool intersection(const Vec3 &originaly, const Vec3 &direction, float &tnear) const{
        float a = v0.get_x() - v1.get_x() , b = v0.get_x() - v2.get_x() , c = direction.get_x() , d = v0.get_x() - originaly.get_x();
        float e = v0.get_y() - v1.get_y() , f = v0.get_y() - v2.get_y() , g = direction.get_y() , h = v0.get_y() - originaly.get_y();
        float i = v0.get_z() - v1.get_z() , j = v0.get_z() - v2.get_z() , k = direction.get_z() , l = v0.get_z() - originaly.get_z();
        float m = f * k - g * j, n = h * k - g * l, p = f * l - h * j;
        float q = g * i - e * k, s = e * j - f * i;
        float inv_denom = 1.0 / ( a * m + b * q + c * s);
        float e1 = d * m - b * n - c * p;
        float beta = e1 * inv_denom;
        if(beta < 0.0)
            return false;
        float r = e * l - h * i;
        float e2 = a * n + d * q + c * r;
        float gamma = e2 * inv_denom;
        if(gamma < 0.0)
            return false;
        if(beta + gamma > 1.0)
            return false;
        float e3 = a * p - b * r + d * s;
        float t = e3 * inv_denom;
        if (t < 1e-9)
            return false;
        tnear =t;
        return true; 
    }  

    void get_data(const Vec3 &target_hit, Vec3 &N, Material &mat) const {
        N = gmath.crossVec((v1 - v0),(v2 - v0));
        N = gmath.normalization(N) * norm;
        mat = material;
    }
};

class Plane : public Object
{
protected:
    Vec3 v0;
    Vec3 n;
    Material material;

public:
    Plane (const Vec3 &v, const Vec3 &n ,const Material &mat) : v0(v), n(n), material(mat){}

    bool intersection(const Vec3 &originaly, const Vec3 &direction, float &tnear) const{
        float t = gmath.scalar((v0 - originaly) , n) / gmath.scalar(direction, n); 											
        if (t < 1e-7){
            return(false);	
        }
        tnear = t;
        return (true);
    }  

    void get_data(const Vec3 &hit_point, Vec3 &N, Material &mat) const{
        N = gmath.normalization(n);
        mat = material;
        mat.set_diffuse_color((int(.25 * hit_point.get_x() + 1000) + int(.25 * hit_point.get_z())) & 1 ? Vec3(1, 1, 1) : Vec3(0, 0, 0));
        mat.set_diffuse_color(mat.get_diffuse_color()*0.5);
    }
};

class StandartLight : public Light{
protected:
    Vec3 position;
    float intensity;
    Vec3 color;

public:
    StandartLight(const Vec3 &pos, const float &intens, const Vec3 &col) : position(pos), intensity(intens), color(col) {}
    void get_data_light(const Vec3 &pos, Vec3 &direction_light, Vec3 &intensity_light, float &distance) const {
        direction_light = (position - pos); 
        float r = gmath.norma(direction_light); 
        distance = sqrt(r); 
        direction_light = gmath.normalization(direction_light);
        intensity_light = color * intensity; 
    }
};